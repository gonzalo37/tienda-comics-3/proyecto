<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dibujan".
 *
 * @property int $id
 * @property int|null $codigo_dibujante
 * @property int|null $codigo_numerico_comic
 *
 * @property Comics $codigoNumericoComic
 * @property Dibujantes $codigoDibujante
 */
class Dibujan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dibujan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_dibujante', 'codigo_numerico_comic'], 'integer'],
            [['codigo_dibujante', 'codigo_numerico_comic'], 'unique', 'targetAttribute' => ['codigo_dibujante', 'codigo_numerico_comic']],
            [['codigo_numerico_comic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['codigo_numerico_comic' => 'codigo_numerico']],
            [['codigo_dibujante'], 'exist', 'skipOnError' => true, 'targetClass' => Dibujantes::className(), 'targetAttribute' => ['codigo_dibujante' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_dibujante' => 'Codigo Dibujante',
            'codigo_numerico_comic' => 'Codigo Numerico Comic',
        ];
    }

    /**
     * Gets query for [[CodigoNumericoComic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComic()
    {
        return $this->hasOne(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic']);
    }

    /**
     * Gets query for [[CodigoDibujante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDibujante()
    {
        return $this->hasOne(Dibujantes::className(), ['codigo' => 'codigo_dibujante']);
    }
}
