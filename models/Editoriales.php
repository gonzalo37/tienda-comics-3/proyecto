<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "editoriales".
 *
 * @property int $codigo
 * @property string|null $logotipo
 * @property string|null $nombre
 * @property string|null $descripción
 *
 * @property Comics[] $comics
 */
class Editoriales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'editoriales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logotipo', 'descripción'], 'string', 'max' => 1000],
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'logotipo' => 'Logotipo',
            'nombre' => 'Nombre',
            'descripción' => 'Descripción',
        ];
    }

    /**
     * Gets query for [[Comics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_editorial' => 'codigo']);
    }
}
