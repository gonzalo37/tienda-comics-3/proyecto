<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comics".
 *
 * @property int $codigo_numerico
 * @property string|null $nombre
 * @property string|null $coleccion
 * @property int|null $n_dibujante
 * @property string|null $nombre_dibujante
 * @property int|null $codigo_editorial
 * @property string|null $portada
 * @property string|null $descripción
 * @property int|null $valoracion
 *
 * @property Editoriales $codigoEditorial
 * @property Dibujan[] $dibujans
 * @property Dibujantes[] $codigoDibujantes
 * @property Lee[] $lees
 * @property Usuarios[] $codigoUsuarios
 * @property Vende[] $vendes
 */
class Comics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_editorial', 'valoracion'], 'integer'],
            [['nombre', 'coleccion'], 'string', 'max' => 100],
            [['nombre_dibujante'], 'string', 'max' => 50],
            [['portada'], 'string', 'max' => 1000],
            [['descripción'], 'string', 'max' => 10000],
            [['codigo_editorial'], 'exist', 'skipOnError' => true, 'targetClass' => Editoriales::className(), 'targetAttribute' => ['codigo_editorial' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_numerico' => 'Codigo Numerico',
            'nombre' => 'Nombre',
            'coleccion' => 'Coleccion',
            'nombre_dibujante' => 'Nombre Dibujante',
            'codigo_editorial' => 'Codigo Editorial',
            'portada' => 'Portada',
            'descripción' => 'Descripción',
            'valoracion' => 'Valoracion',
        ];
    }

    /**
     * Gets query for [[CodigoEditorial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEditorial()
    {
        return $this->hasOne(Editoriales::className(), ['codigo' => 'codigo_editorial']);
    }

    /**
     * Gets query for [[Dibujans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDibujans()
    {
        return $this->hasMany(Dibujan::className(), ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[CodigoDibujantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDibujantes()
    {
        return $this->hasMany(Dibujantes::className(), ['codigo' => 'codigo_dibujante'])->viaTable('dibujan', ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[Lees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLees()
    {
        return $this->hasMany(Lee::className(), ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[CodigoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['codigo' => 'codigo_usuario'])->viaTable('lee', ['codigo_numerico_comic' => 'codigo_numerico']);
    }

    /**
     * Gets query for [[Vendes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendes()
    {
        return $this->hasMany(Vende::className(), ['codigo_numerico_comic' => 'codigo_numerico']);
    }
}
