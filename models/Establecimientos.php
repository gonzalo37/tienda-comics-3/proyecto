<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "establecimientos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $dirección
 * @property string|null $imagen_establecimiento
 * @property int|null $valoracion
 *
 * @property Telefonos[] $telefonos
 * @property Vende[] $vendes
 */
class Establecimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'establecimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valoracion'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['dirección', 'imagen_establecimiento'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'dirección' => 'Dirección',
            'imagen_establecimiento' => 'Imagen Establecimiento',
            'valoracion' => 'Valoracion',
        ];
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['codigo_establecimiento' => 'codigo']);
    }

    /**
     * Gets query for [[Vendes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendes()
    {
        return $this->hasMany(Vende::className(), ['codigo_establecimiento' => 'codigo']);
    }
}
