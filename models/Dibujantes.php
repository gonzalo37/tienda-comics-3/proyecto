<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dibujantes".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $apellido
 * @property string|null $biografia
 * @property string|null $foto
 *
 * @property Dibujan[] $dibujans
 * @property Comics[] $codigoNumericoComics
 */
class Dibujantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dibujantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
            [['apellido'], 'string', 'max' => 20],
            [['biografia', 'foto'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'biografia' => 'Biografia',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Dibujans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDibujans()
    {
        return $this->hasMany(Dibujan::className(), ['codigo_dibujante' => 'codigo']);
    }

    /**
     * Gets query for [[CodigoNumericoComics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic'])->viaTable('dibujan', ['codigo_dibujante' => 'codigo']);
    }
}
