<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Lee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'leido')->widget(SwitchInput::classname(), []); ?>

    <?= $form->field($model, 'favorito')->widget(SwitchInput::classname(), []); ?>

    <div class="form-group">  
        <?= Html::resetButton('Restablecer', ['class' => 'btn btn-light']) ?>


        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
