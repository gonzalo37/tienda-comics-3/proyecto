<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lee */

?>
<div class="lee-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
