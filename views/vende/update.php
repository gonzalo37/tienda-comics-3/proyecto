<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vende */

$this->title = 'Update Vende: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vendes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vende-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
