<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vende */

$this->title = 'Create Vende';
$this->params['breadcrumbs'][] = ['label' => 'Vendes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vende-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
