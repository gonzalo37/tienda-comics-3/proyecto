<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vende-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vende', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'codigo_establecimiento',
            'codigo_numerico_comic',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
