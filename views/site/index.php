<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'ComicList';  //ponemos el titulo de la página de inicio
// aqui modificamos el aspecto de la página principal
?>
<div class="site-index">
    <div class="cuadrado">
        <div><?= Html::img('@web/images/34053.jpg', ['class' => 'logo']) ?></div>
        <div class="texto_cuadrado">
            <p class="parrafo_cuadrado">Mira toda la información que te ofrece ComicList</p>
        </div>
    </div>  
    <div class="cuadrado2">
        <div class="c"><div class="linea_separadora">
                <div class="linea_separadora2"><div class="linea_separadora3"><div class="linea_separadora4"></div></div></div>  
            </div>  
        </div>
        <div class="texto_linea"><p class="novedades">NOVEDADES</p></div>
        <div class="celda1">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <?= Html::img('@web/images/novedades1.jpg', ['class' => 'novedades1']) ?>
                    <div class="caption">
                        <h3 class="titulo1">Marvel Integral. Capitán América: El soldado de invierno</h3>
                        <p class="parrafo1">Reimpresión: Marvel Integral. Capitán América: El soldado de invierno.El tomo ya está de nuevo disponible en tu librería de referencia y en nuestra web.</p>
                        <p><a href="https://www.panini.es/shp_esp_es/marvel-integral-capit-n-am-rica-el-soldado-de-invierno-smain001-es01.html" class="btn btn-primary" role="button">Acceder a Panini Comics</a></p>
                    </div>
                </div>
            </div>
        </div> 
        <div class="celda2">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <?= Html::img('@web/images/novedades2.jpg', ['class' => 'novedades2']) ?>
                    <div class="caption">
                        <h3 class="titulo2">Las Tortugas Ninja: Avance Editorial 2021</h3>
                        <p class="parrafo2">La Tortugamanía ha llegado para quedarse. Además de los títulos en curso, en el segundo semestre del año publicaremos la esperada miniserie Las Tortugas Ninja: El último Ronin y el especial Las Tortugas Ninja de Richard Corben.</p>
                        <p><a href="https://www.ecccomics.com/contenidos/las-tortugas-ninja-avance-editorial-2021-12233.aspx" class="btn btn-primary" role="button">Acceder a ECC Comics</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="celda3">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <?= Html::img('@web/images/novedades3.jpg', ['class' => 'novedades3']) ?>
                    <div class="caption">
                        <h3 class="titulo3">Marvel Comics: 60 años de superhéroes</h3>
                        <p class="parrafo3">Con una exposición llena de rarezas, la Casa de América y el festival de cómics de Múnich celebran el 60 aniversario de Marvel, la editorial de cómics de culto de Estados Unidos.</p>
                        <p><a href="https://www.dw.com/es/marvel-comics-60-a%C3%B1os-de-superh%C3%A9roes/a-57745947" class="btn btn-primary" role="button">Accede a DW</a> </p>
                    </div>
                </div>
            </div>
        </div> 
        <div class="celda4">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <?= Html::img('@web/images/novedades4.jpg', ['class' => 'novedades4']) ?>
                    <div class="caption">
                        <h3 class="titulo4">Marvel Comics revela una nueva serie de Hulk que promete elevar el nivel de destrucción</h3>
                        <p class="parrafo4">Tras la noticia de que Venom tendrá una nueva serie que continuará su historia en noviembre, Marvel Comics también ha revelado una nueva serie de comics para Hulk. Resulta que el actual guionista de Immortal Hulk, Al Ewing, y el guionista de Venom, Donny Cates, cambiar...</p>
                        <p><a href="https://es.ign.com/marvel-comics/173710/news/marvel-comics-revela-una-nueva-serie-de-hulk-que-promete-elevar-el-nivel-de-destruccion" class="btn btn-primary" role="button">Accede a IGN España</a>
                    </div>
                </div>
            </div>
            <div class="celda5">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <?= Html::img('@web/images/novedades5.jpg', ['class' => 'novedades5']) ?>
                        <div class="caption">
                            <h3 class="titulo5">DC Comics revive Fábulas con ¡Batman vs. Bigby! Un lobo en Gotham, y luego continuará donde lo dejaron en 2015</h3>
                            <p class="parrafo5">Han pasado casi 20 años desde que DC Comics lanzó por primera vez Fábulas. </p>
                            <p><a href="https://es.ign.com/dc-comics/173899/news/dc-comics-revive-fabulas-con-batman-vs-bigby-un-lobo-en-gotham-y-luego-continuara-donde-lo-dejaron-e" class="btn btn-primary" role="button">Accede a IGN España</a></p>
                        </div>
                    </div>
                </div>     
            </div>     
        </div> 
        <div class="celda6">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <?= Html::img('@web/images/novedades6.png', ['class' => 'novedades6']) ?>
                    <div class="caption">
                        <h3 class="titulo6">ESTADO FUTURO: AVANCE EDITORIAL 2021</h3>
                        <p class="parrafo6">La nueva fase del Universo DC llega este septiembre. Una alineación estelar de escritores y dibujantes nos presentarán a los superhéroes DC del futuro y a nuevos supervillanos en este evento de dos meses de duración.</p>
                        <p><a href="https://www.ecccomics.com/contenidos/estado-futuro-avance-editorial-2021-12238.aspx" class="btn btn-primary" role="button">Accede a ECC Comics</a></p>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="cuadrado3">
        <p class="parrafo_cuadrado3">Ahora podrás tener acceso a las páginas web de nuestros patrocinadores</p>
        <a href="https://www.panini.es/">
            <?= Html::img('@web/images/panini.png', ['class' => 'panini']) ?>
        </a>
        <a href="https://www.dccomics.com/">
            <?= Html::img('@web/images/dc.png', ['class' => 'dc']) ?>
        </a>
        <a href="https://www.idwpublishing.com/">
            <?= Html::img('@web/images/idw.png', ['class' => 'idw']) ?>
        </a>
        <a href="https://www.marvel.com/">
            <?= Html::img('@web/images/marvel.png', ['class' => 'marvel']) ?>
        </a>
        <a href="https://www.normacomics.com/">
            <?= Html::img('@web/images/norma.png', ['class' => 'norma']) ?>
        </a>
        <a href="https://imagecomics.com/">
            <?= Html::img('@web/images/image.png', ['class' => 'image']) ?>
        </a>
        <a href="https://www.ecccomics.com/">
            <?= Html::img('@web/images/ecc.jpg', ['class' => 'ecc']) ?>
        </a>
    </div>



</div>