<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre nosotros';

?>
<div class="site-about">
   
   <div class="cuadrado_about">  
         <?= Html::img('@web/images/logo.png', ['class' => 'logotipo_about']) ?>
        <div class="titulo_about">ComicList</div>
        <div class="parrafo_about">ComicList es una aplicación a partir de una iniciativa promovida por editoriales de comics y sus empresas distribuidoras de comics. Necesitaban una aplicación 
            en la cual los lectores 
            habituales de comics pudieran valorar, incluir y modificar información sobre comics
            y todo lo relacionado con ello. Asi tanto las editoriales y sus distribuidoras tendrían una opinión veraz de sus comics. </div>
    </div>
</div>
