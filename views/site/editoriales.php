<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$titulo = "EDITORIALES";
?>

<h2 style="text-align: center; max-height: 50px; color=#330066"><?= $titulo ?></h2>
<div class="linea_recta"></div>
<div class="texto_titulo">En esta sección podreís visualizar la informción de todas las editoriales, también teneis la opción de incluir vuestras propias sugerencias incluyendo la editorial que querais, para compartirlo con el resto de usuarios.</div>

<p>

<?= Html::a('Introducir nueva editorial', ['editoriales/create'], ['class' => 'btn btn-warning']) ?>
</p>


<div class="row">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_editorial',
        'layout' => "{pager}\n{items}",
    ]);
    ?>   

</div>
