<?php

use yii\helpers\Html;

$this->title = "Editoriales";   //ponemos el titulo que aparece en el head
?>

<div class="caja">
    <div class="col-md-12">    
        <!--        con col-md modulamos en que tipo de formato lo queremos-->
        <div class="thumbnail">

            <div class="caption">
                <?=Html::img($model->logotipo) ?>
                <h3><?= $model->nombre ?></h3>
                <!--  pongo el dato procedente del modelo al que procede en este caso al de la editorial    -->
                <p><?= $model->descripción?> </p>
                <?= Html::a('Actualizar', ['editoriales/update', 'id' => $model->codigo], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a('Eliminar', ['editoriales/delete', 'id' => $model->codigo], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro de querer borrar este elemento?',
                        'method' => 'post',
                    //en los botones de actualizar eliminar ponemos "editoriales/update" y "editoriales/delete" y especificamos el botón. Para enlazarlos con los archivos php que realizan estas acciones, eso si en las views correspondientes
                    //al ser comics necesita la id con la que se identifica, en este caso código
                    ],
                ])
                ?>       
            </div>
        </div>
    </div>
</div>

