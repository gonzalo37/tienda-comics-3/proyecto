<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujantes */

$this->title = 'Introduce un nuevo dibujante: ';

?>
<div class="dibujantes-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
