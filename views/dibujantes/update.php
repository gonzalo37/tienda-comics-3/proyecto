<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujantes */

$this->title = 'Actualiza la información de la editorial: ' . $model->nombre;

?>
<div class="dibujantes-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
