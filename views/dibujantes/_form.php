<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dibujantes-form">
<?php $form = ActiveForm::begin() ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'nombre' => ['label' => 'Nombre del dibujante', 'options' => ['placeholder' => 'Ej']],
            'apellido' => ['label' => 'Apellido del dibujante', 'options' => ['placeholder' => 'Ej']],
        ]
    ])
    ?>
    <?=
    Form::widget([// continuation fields to row above without labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'biografia' => ['label' => 'Biografía del dibujante','options' => ['placeholder' => 'Introduce la biografía del dibujante']],
        ]
    ])
    ?>
    <?=
        Form::widget([// continuation fields to row above without labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'foto' => ['label' => 'Fotografía del dibujante', 'options' => ['placeholder' => 'Ejemplo:@web/images/fotodeldibujante.jpg']],
        ]
    ])
        
        ?>
    <div class="form-group">  
        <?= Html::resetButton('Restablecer', ['class' => 'btn btn-light']) ?>


        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>

</div>
