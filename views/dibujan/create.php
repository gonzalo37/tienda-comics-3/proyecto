<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujan */

$this->title = 'Create Dibujan';
$this->params['breadcrumbs'][] = ['label' => 'Dibujans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dibujan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
