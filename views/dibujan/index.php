<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dibujans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dibujan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dibujan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'codigo_dibujante',
            'codigo_numerico_comic',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
