<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dibujan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dibujan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_dibujante')->textInput() ?>

    <?= $form->field($model, 'codigo_numerico_comic')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
