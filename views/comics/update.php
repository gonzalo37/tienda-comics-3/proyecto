<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = 'Update Comics: ' . $model->nombre;

?>
<div class="comics-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
