<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
$titulo="COMICS";   //ponemos el titulo que aparece en el head
?>


<h2 style="text-align: center; max-height: 50px;"><?= $titulo ?></h2>
<div class="linea_recta"></div>
<div class="texto_titulo">En esta sección podreís visualizar todos los comics disponibles a valorar, también teneis la opción de incluir vuestras propias sugerencias incluyendo el comic que querais, para compartirlo con el resto de usuarios.</div>
<p>
    
    <?=Html::a('Introducir nuevo comic',['comics/create'],['class' => 'btn btn-warning'])?>
</p>


<div class="row">                                         
    <?= ListView::widget([            //con esta parte de código cambiamos la vista de un Grid View a un ListView
       'dataProvider' => $dataProvider,
        'itemView'=>'_comics',         // vinculamos este archivo php con otro
        'layout'=>"{pager}\n{items}",
        
        
        
    ]);
   ?>   
    
</div>

