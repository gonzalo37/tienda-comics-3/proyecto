<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\rating\StarRating;
use kartik\switchinput\SwitchInput;
use yii\widgets\ActiveForm;
use app\models\Lee;
$this->title = "Comics";    //ponemos el titulo que aparece en el head
                                                                 //pongo el Html::img, forma parte de bootstrap con lo que puedo implementar una imagen donde que quiera
?>
                                                                 
<div class="caja">
    <div class="col-md-12">
        <!--        con col-md modulamos en que tipo de formato lo queremos-->
        <div class="thumbnail">
           <?=Html::img($model->portada) ?>                                 
            <div class="caption">
                <h2 class="nombreComic"><?= $model->nombre?>: <?= $model->coleccion?></h2>
                <h3 class="nombreDib"><?= $model->nombre_dibujante?></h3>
<!--  pongo el dato procedente del modelo al que procede en este caso al de los comics    -->
                <p class="descComic"><?= $model->descripción ?> </p>
                
                
                <?php $form = ActiveForm::begin(); ?>
                
                <p class="StarRating">
                     <?=
                    StarRating::widget([
    'name' => 'rating_35',
    'value' =>$model->valoracion ,
    'pluginOptions' => ['displayOnly' => true,
        'filledStar' => '&#x2605;',
        'emptyStar' => '&#x2606;']
]);
                    ?>
                    
     
                </p>

                <?php ActiveForm::end(); ?>
                  
                     <?= Html::a('Actualizar', ['comics/update', 'id' => $model->codigo_numerico], ['class' => 'btn btn-primary']) ?>   
                     <?= Html::a('Eliminar', ['comics/delete', 'id' => $model->codigo_numerico], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer borrar este elemento?',
                'method' => 'post',
            ],                   //en los botones de actualizar eliminar ponemos "comics/update" y "comics/delete" para enlazarlos con los archivos php que realizan estas acciones, eso si en las views correspondientes
                                //al ser comics necesita la id con la que se identifica, en este caso código_numérico
                         ]) ?> 
                    <?= Html::a('Leído/Favorito', ['lee/view', 'id' => $model->codigo_numerico], ['class' => 'btn btn-info']) ?> 
                </div>               
            </div>
        </div>
    </div>
</div>

