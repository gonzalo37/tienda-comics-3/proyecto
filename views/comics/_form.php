<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\rating\StarRating;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comics-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'nombre' => ['label' => 'Título del comic', 'options' => ['placeholder' => 'Introduce título del comic']],
            'nombre_dibujante' => ['label' => 'Nombre del dibujante', 'options' => ['placeholder' => 'Introduce el nombre del dibujante']],
        ]
    ])
    ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'coleccion' => ['label' => 'Colección', 'options' => ['placeholder' => 'Introduce la colección a la que pertenece el comic']],
            'codigo_editorial' => ['label' => 'Código de la editorial', 'options' => ['placeholder' => 'Introduce el código de la editorial: Marvel->1, DC->2, Image Comics->4']],
        ]
    ])
    ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'portada' => ['label' => 'Portada del comic', 'options' => ['placeholder' => 'Introduce la portada del comic,ejemplo:@carpeta/carpeta/foto.jpg']],
        ]
    ])
    ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'descripción' => ['label' => 'Descripción del comic', 'options' => ['placeholder' => 'Introduce la sinopsis del comic']],
        ]
    ])
    ?>

    <?=
    $form->field($model, 'valoracion')->widget(StarRating::classname(), [
        'pluginOptions' => ['step' => 1,
            'filledStar' => '&#x2605;',
            'emptyStar' => '&#x2606;',
        ]
    ]);
    ?>


    <div class="form-group">
         <?= Html::resetButton('Restablecer', ['class' => 'btn btn-light']) ?>
        
        
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
