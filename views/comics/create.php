<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = 'Introduce un nuevo comic';

?>
<div class="comics-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
