<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editoriales */

$this->title = 'Actualiza la información del dibujante: ' . $model->nombre;

?>
<div class="editoriales-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
