<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\Editoriales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="editoriales-form">
<?php $form = ActiveForm::begin() ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'nombre' => ['label' => 'Nombre de la editorial', 'options' => ['placeholder' => 'Ej']],
            'descripción' => ['label' => 'Descripción de la editorial', 'options' => ['placeholder' => 'Ej']],
        ]
    ])
    ?>
    <?=
    Form::widget([// continuation fields to row above without labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'logotipo' => ['label' => 'Logotipo de la editorial','options' => ['placeholder' => 'Introducir la ruta de la carpeta donde esta guardada la imagen. Ejemplo:@web/images/foto.jpg']],
        ]
    ])
    ?>
   <div class="form-group">  
        <?= Html::resetButton('Restablecer', ['class' => 'btn btn-light']) ?>


        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
