<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Establecimientos */

$this->title = 'Actualiza la información del establecimiento: ' . $model->nombre;

?>
<div class="establecimientos-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
