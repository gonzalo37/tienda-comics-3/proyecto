<?php

use yii\helpers\Html;
use kartik\rating\StarRating;
use yii\widgets\ActiveForm;


$this->title = "Establecimientos";   //ponemos el titulo que aparece en el head
?>

<div class="caja">
    <div class="col-md-12">
        <!--        con col-md modulamos en que tipo de formato lo queremos-->
        <div class="thumbnail">
         
            <div class="caption">
                <?=Html::img($model->imagen_establecimiento) ?>
                <h3><?= $model->nombre?></h3>
                <!--  pongo el dato procedente del modelo al que procede en este caso al del establecimiento    -->
                <p>Dirección: <?= $model->dirección ?> </p>
                 
                <p class="StarRating">
                     <?=
                    StarRating::widget([
    'name' => 'rating_35',
    'value' =>$model->valoracion ,
    'pluginOptions' => ['displayOnly' => true,
        'filledStar' => '&#x2605;',
        'emptyStar' => '&#x2606;']
]);
                    ?>
                    
     
                </p>
      <?= Html::a('Actualizar', ['establecimientos/update', 'id' => $model->codigo], ['class' => 'btn btn-primary']) ?>
                     <?= Html::a('Eliminar', ['establecimientos/delete', 'id' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer borrar este elemento?',
                'method' => 'post',
            ],//en los botones de actualizar eliminar ponemos "establecimientos/update" y "establecimientos/delete" y especificamos el botón. Para enlazarlos con los archivos php que realizan estas acciones, eso si en las views correspondientes
                                //al ser comics necesita la id con la que se identifica, en este caso código
        ]) ?>       
            </div>
        </div>
    </div>
</div>



