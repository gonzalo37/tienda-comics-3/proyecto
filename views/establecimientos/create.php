<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Establecimientos */

$this->title = 'Introduce un nuevo establecimiento'; //el titulo de la sección para crear un nuevo establecimiento

?>
<div class="establecimientos-create">

    <h2><?= Html::encode($this->title) ?></h2>   
    
    <?= $this->render('_form', [
        'model' => $model,         //Renderiza en los modelos que se encuentran en el formulario de establecimientos
    ]) ?>

</div>
