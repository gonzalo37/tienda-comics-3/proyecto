<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Establecimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="establecimientos-form"> 
    <?php $form = ActiveForm::begin() ?>
    <?=
    Form::widget([// fields with labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'nombre' => ['label' => 'Nombre del establecimiento', 'options' => ['placeholder' => 'Introduce nombre del establecimiento']],
            'dirección' => ['label' => 'Dirección del establecimiento', 'options' => ['placeholder' => 'Introduce dirección del establecimiento']],
        ]
    ])
    ?>
    <?=
    Form::widget([// continuation fields to row above without labels
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'imagen_establecimiento' => ['label' => 'Foto del establecimiento', 'options' => ['placeholder' => 'Introduce la imagen del establecimiento,ejemplo:@carpeta/carpeta/foto.jpg']],
        ]
    ])
    ?>
    <?=
    $form->field($model, 'valoracion')->widget(StarRating::classname(), [
        'pluginOptions' => ['step' => 1,
            'filledStar' => '&#x2605;',
            'emptyStar' => '&#x2606;',
        ]
    ]);
    ?>

    <div class="form-group">  
        <?= Html::resetButton('Restablecer', ['class' => 'btn btn-light']) ?>


        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>





