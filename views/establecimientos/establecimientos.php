<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$titulo = "ESTABLECIMIENTOS";             //ponemos el titulo que aparece en el head
?>

<h2 style="text-align: center; max-height: 50px;"><?= $titulo ?></h2>
<div class="linea_recta"></div>
<div class="texto_titulo">En esta sección podreís visualizar la informción de todas los establecimientos, también teneis la opción de incluir vuestras propias sugerencias incluyendo la editorial que querais, para compartirlo con el resto de usuarios.</div>
<p>

<p>

<?= Html::a('Introducir nuevo establecimiento', ['establecimientos/create'], ['class' => 'btn btn-warning']) ?>
</p>


<div class="row">
    <?=
    ListView::widget([                    //con esta parte de código cambiamos la vista de un Grid View a un ListView
        'dataProvider' => $dataProvider,
        'itemView' => '_establecimiento',      // vinculamos este archivo php con otro
        'layout' => "{pager}\n{items}",
    ]);
    ?>   

</div>
